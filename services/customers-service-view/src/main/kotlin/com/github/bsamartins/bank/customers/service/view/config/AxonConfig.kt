package com.github.bsamartins.bank.customers.service.view.config

import org.axonframework.boot.autoconfig.AxonAutoConfiguration
import org.axonframework.boot.autoconfig.KafkaProperties
import org.axonframework.config.Configurer
import org.axonframework.config.DefaultConfigurer
import org.axonframework.config.EventProcessingConfiguration
import org.axonframework.kafka.eventhandling.DefaultKafkaMessageConverter
import org.axonframework.kafka.eventhandling.KafkaMessageConverter
import org.axonframework.kafka.eventhandling.consumer.*
import org.axonframework.serialization.Serializer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.AutoConfigureAfter
import org.springframework.boot.autoconfigure.AutoConfigureBefore
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit


@Configuration
@EnableConfigurationProperties(KafkaProperties::class)
@AutoConfigureAfter(AxonAutoConfiguration::class)
open class AxonConfig(private val kafkaProperties: KafkaProperties,
                      @Qualifier("eventSerializer") private val eventSerializer: Serializer) {

    @Bean(destroyMethod = "shutdown")
    open fun axonKafkaFetcher(): Fetcher<String, ByteArray> {
        return AsyncFetcher.builder(axonKafkaConsumerFactory())
                .withTopic(kafkaProperties.defaultTopic)
                .withPollTimeout(kafkaProperties.fetcher.pollTimeout, TimeUnit.MILLISECONDS)
                .withMessageConverter(axonKafkaMessageConverter())
                .withBufferFactory { SortedKafkaMessageBuffer(kafkaProperties.fetcher.bufferSize) }
                .build()
    }

    @Bean
    open fun axonKafkaTrackingToken(kafkaMessageSource: KafkaMessageSource<String, ByteArray>): KafkaTrackingToken = KafkaTrackingToken.emptyToken()

    @ConditionalOnMissingBean
    @Bean
    open fun axonKafkaMessageSource(): KafkaMessageSource<String, ByteArray> {
        return KafkaMessageSource(axonKafkaFetcher())
    }

    @Bean
    @ConditionalOnProperty("axon.kafka.consumer.group-id")
    open fun axonKafkaConsumerFactory(): ConsumerFactory<String, ByteArray> {
        return DefaultConsumerFactory(kafkaProperties.buildConsumerProperties())
    }

    @Bean
    open fun axonKafkaMessageConverter(): KafkaMessageConverter<String, ByteArray> {
        return DefaultKafkaMessageConverter(eventSerializer)
    }
}