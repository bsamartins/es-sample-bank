package com.github.bsamartins.bank.customers.service.view

import com.github.bsamartins.bank.customers.common.event.CustomerCreatedEvent
import com.github.bsamartins.bank.customers.service.view.model.Customer
import org.axonframework.eventhandling.EventHandler
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class CustomerEventHandler {

    private val log = LoggerFactory.getLogger(CustomerEventHandler::class.java)

    @Autowired
    private lateinit var customerService: CustomerService

    @EventHandler
    fun on(event: CustomerCreatedEvent): Customer {
        log.info("Got event: {}", event)
        val user = Customer()
        user.id = event.id
        user.email = event.email
        user.name = event.name
        user.password = event.password

        return customerService.create(user)
    }
}