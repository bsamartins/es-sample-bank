package com.github.bsamartins.bank.customers.service.view

import com.github.bsamartins.bank.customers.service.view.model.Customer
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface CustomerRepository: MongoRepository<Customer, String>, QuerydslPredicateExecutor<Customer>