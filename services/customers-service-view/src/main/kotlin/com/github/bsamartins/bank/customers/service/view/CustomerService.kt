package com.github.bsamartins.bank.customers.service.view

import com.github.bsamartins.bank.customers.service.view.model.Customer
import com.querydsl.core.types.Predicate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class CustomerService {

    @Autowired
    private lateinit var customerRepository: CustomerRepository

    fun create(customer: Customer): Customer = customerRepository.save(customer)

    fun findAll(predicate: Predicate, pageable: Pageable): Page<Customer> {
        return customerRepository.findAll(predicate, pageable)
    }
}