package com.github.bsamartins.bank.customers.service.view

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@SpringBootApplication(exclude=[KafkaAutoConfiguration::class])
@EnableMongoRepositories
@EnableDiscoveryClient
open class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}