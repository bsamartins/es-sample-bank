package com.github.bsamartins.bank.customers.service.view

import com.github.bsamartins.bank.customers.service.view.model.Customer
import com.querydsl.core.types.Predicate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.querydsl.binding.QuerydslPredicate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/customers")
class CustomerController {

    @Autowired
    private lateinit var customerService: CustomerService

    @GetMapping
    fun findCustomers(@QuerydslPredicate(root = Customer::class) predicate: Predicate, pageable: Pageable): Page<Customer> {
        return customerService.findAll(predicate, pageable)
    }

}