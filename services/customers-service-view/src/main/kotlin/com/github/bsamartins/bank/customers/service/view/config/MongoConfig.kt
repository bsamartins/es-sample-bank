package com.github.bsamartins.bank.customers.service.view.config

import com.mongodb.MongoClient
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoConfiguration

@Configuration
open class MongoConfig: AbstractMongoConfiguration() {

    @Value("\${mongodb.hostname}")
    private lateinit var hostname: String

    @Value("\${mongodb.database}")
    private lateinit var mongoDatabase: String

    override fun mongoClient(): MongoClient = MongoClient(hostname)

    override fun getDatabaseName() = mongoDatabase
}