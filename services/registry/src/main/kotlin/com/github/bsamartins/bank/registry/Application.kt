package com.github.bsamartins.bank.registry

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
open class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}