package com.github.bsamartins.bank.customers.common.event

data class CustomerCreatedEvent(
        var id: String? = null,
        var name: String? = null,
        var email: String? = null,
        var password: String? = null): CustomerEvent() {

    constructor(): this(null, null, null, null)
}