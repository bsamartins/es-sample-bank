package com.github.bsamartins.bank.customers.service

import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.CompletableFuture

@RestController
@RequestMapping("/api/v1/customers")
class CustomerController(private val customerService: CustomerService) {

    @PostMapping
    fun create(@Validated @RequestBody customerCreate: CustomerCreate): CompletableFuture<String> {
        return this.customerService.create(customerCreate)
    }

}