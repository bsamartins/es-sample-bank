package com.github.bsamartins.bank.customers.service.aggregate

import com.github.bsamartins.bank.customers.common.event.CustomerCreatedEvent
import com.github.bsamartins.bank.customers.service.command.CreateCustomerCommand
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.commandhandling.model.AggregateIdentifier
import org.axonframework.commandhandling.model.AggregateLifecycle.apply
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.spring.stereotype.Aggregate
import org.slf4j.LoggerFactory

@Aggregate
class CustomerAggregate {

    private val log = LoggerFactory.getLogger(CustomerAggregate::class.java)

    @AggregateIdentifier
    var id: String? = null
        private set
    var name: String? = null
        private set
    var email: String? = null
        private set
    var password: String? = null
        private set

    @CommandHandler
    constructor(cmd: CreateCustomerCommand) {
        log.info("Calling CustomerAggregate.process for CreateCustomerCommand: {}", cmd)
        apply(CustomerCreatedEvent(cmd.id, cmd.name, cmd.email, cmd.password))
    }

    @EventSourcingHandler
    fun on(event: CustomerCreatedEvent) {
        log.info("Got event: {}", event)
        this.id = event.id
        this.name = event.name
        this.email = event.email
        this.password = event.password
    }
}
