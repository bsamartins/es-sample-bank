package com.github.bsamartins.bank.customers.service

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
open class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}