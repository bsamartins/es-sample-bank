package com.github.bsamartins.bank.customers.service

import com.github.bsamartins.bank.customers.service.command.CreateCustomerCommand
import org.axonframework.commandhandling.gateway.CommandGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.util.Assert
import java.util.*
import java.util.concurrent.CompletableFuture

@Service
class CustomerService(private val commandGateway: CommandGateway) {

    @Autowired
    private lateinit var customerQueryService: CustomerQueryService

    fun create(customer: CustomerCreate): CompletableFuture<String> {
        return customerQueryService.isEmailUnique(customer.email).thenCompose { isUnique ->
            Assert.isTrue(isUnique, "Email not unique")

            return@thenCompose this.commandGateway.send<String>(
                    CreateCustomerCommand(
                            UUID.randomUUID().toString(),
                            customer.email,
                            customer.password,
                            customer.name))
        }
    }
}