package com.github.bsamartins.bank.customers.service.command

import org.axonframework.commandhandling.TargetAggregateIdentifier

data class CreateCustomerCommand(
        @TargetAggregateIdentifier
        val id: String,
        val email: String,
        val password: String,
        val name: String): CustomerCommand