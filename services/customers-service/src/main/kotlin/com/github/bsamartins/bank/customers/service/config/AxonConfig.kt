package com.github.bsamartins.bank.customers.service.config

import com.mongodb.MongoClient
import org.axonframework.mongo.DefaultMongoTemplate
import org.axonframework.mongo.MongoTemplate
import org.axonframework.mongo.eventsourcing.eventstore.MongoEventStorageEngine
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableDiscoveryClient
open class AxonConfig {

    @Bean
    open fun mongoTemplate(mongoClient: MongoClient): MongoTemplate {
        return DefaultMongoTemplate(MongoClient())
    }

    @Bean
    open fun mongoEventStorageEngine(mongoTemplate: MongoTemplate): MongoEventStorageEngine {
        return MongoEventStorageEngine(mongoTemplate)
    }
}