package com.github.bsamartins.bank.customers.service

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import java.util.concurrent.CompletableFuture


@Service
open class CustomerQueryService(private val customersViewClient: CustomersViewClient) {

    open fun isEmailUnique(email: String): CompletableFuture<Boolean> {
        val result = customersViewClient.findAll(email)
        return CompletableFuture.completedFuture(result.content.isEmpty())
    }
}

@FeignClient("customers-service-view")
interface CustomersViewClient {

    @GetMapping("/api/v1/customers")
    fun findAll(@RequestParam("email") email: String?): Page<Customer>

    @GetMapping("/api/v1/customers")
    fun findAllX(@RequestParam("email") email: String?): String

}

data class Page<T> (val content: List<T>,
                    var pageable: Pageable,
                    var last: Boolean,
                    var totalPages: Long,
                    var totalElements: Long,
                    var size: Long,
                    var number: Long,
                    var sort: Sort,
                    var numberOfElements: Long,
                    var first: Boolean)

data class Pageable(val offset: Long,
                    val pageSize: Long,
                    val pageNumber: Long,
                    val paged: Boolean,
                    val unpaged: Boolean,
                    val sort: Sort)

data class Sort(val sorted: Boolean,
                val unsorted: Boolean)

class Customer {
    var name: String? = null
    var email: String? = null
}